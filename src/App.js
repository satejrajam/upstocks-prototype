import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getHistoricalDataAction } from "./actions/historical.actions";
import "./App.css";
import "./App.scss";
import Header from "./components/Header/Header";
import NavBar from "./components/NavBar/NavBar";
import { isMobile } from "react-device-detect";
import { Switch, Route } from "react-router-dom";

import Dashboard from "./pages/Dashboard/Dashboard";
import LiveStocks from "./pages/LiveStocks/LiveStocks";

const io = require("socket.io-client");
const clientSocket = io("http://kaboom.rksv.net/watch");

function App(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getHistoricalDataAction());
  });

  useEffect(() => {
    clientSocket.on("data", function () {
      // console.log("satej connected");
      // clientSocket.emit("ping", {});
      // clientSocket.emit("sub", { state: true });
      // clientSocket.emit('unsub', { state: false })
    });
  }, []);

  // useEffect(() => {
  //   clientSocket.on("data", function (data) {
  //     console.log("satej data", data);
  //     clientSocket.emit("ping", {});
  //   });
  // }, []);

  // useEffect(() => {
  //   clientSocket.on("error", function (error) {
  //     console.log("satej error", error);
  //   });
  // }, []);

  // useEffect(() => {
  //   console.log(test);
  //   console.log("saet");
  // }, [test]);

  return (
    <div className="App">
      <Header />
      <div className="grid-container">
        {!isMobile && (
          <div id="grid-left">
            <NavBar isMobile={isMobile} />
          </div>
        )}
        <div id="grid-right">
          <Switch>
            <Route exact path="/">
              <Dashboard />
            </Route>
            <Route exact path="/live">
              <LiveStocks />
            </Route>
          </Switch>
        </div>
        {isMobile && (
          <div id="footer">
            <NavBar isMobile={isMobile} />
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
