import React from "react";
import "./NavBar.scss";

import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <div id="navBar">
      <div>
        <NavLink
          activeClassName="isActiveLink"
          className="navbar-link"
          exact
          to="/"
        >
          <i className="fas fa-tachometer-alt"></i>
          Overview
        </NavLink>
      </div>
      <div>
        <NavLink
          activeClassName="isActiveLink"
          className="navbar-link"
          exact
          to="/live"
        >
          <i className="fas fa-chart-line"></i>
          Live Chart
        </NavLink>
      </div>
    </div>
  );
};

export default NavBar;
