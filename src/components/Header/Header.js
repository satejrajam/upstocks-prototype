import React from 'react';
import './Header.scss';
import logo from '../../assets/logo.svg';

const Header = (props) => {
    return(
        <div id="header">
            <div className="logoWrap">
                <img src={logo} alt="upstox-logo" />
            </div>
        </div>
    )
}

export default Header;