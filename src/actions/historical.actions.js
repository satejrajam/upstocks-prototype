export const GET_HISTORICAL_DATA = 'GET_HISTORICAL_DATA';
export const GET_HISTORICAL_DATA_SUCCESS = 'GET_HISTORICAL_DATA_SUCCESS';
export const GET_HISTORICAL_DATA_FAIL = 'GET_HISTORICAL_DATA_FAIL';

const getHistoricalDataAction = (reqPayload) => {
    return {
        type: GET_HISTORICAL_DATA,
        payload: reqPayload
    }
}

const getHistoricalActionSuccess = (resPayload) => {
    return {
        type: GET_HISTORICAL_DATA_SUCCESS,
        payload: resPayload
    }
}
const getHistoricalActionFail = (resPayload) => {
    return {
        type: GET_HISTORICAL_DATA_FAIL,
        payload: resPayload
    }
}

export {
    getHistoricalDataAction,
    getHistoricalActionSuccess,
    getHistoricalActionFail
}