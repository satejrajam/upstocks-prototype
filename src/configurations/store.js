import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import rootReducers from '../reducers/rootReducers';
import sagas from '../sagas/sagas';

const persistConfig = {
    key: 'root',
    storage
}

const persistedReducer = persistReducer(persistConfig, rootReducers);
const sagaMiddleware = createSagaMiddleware();
const middlewares = [logger, sagaMiddleware];

let store = createStore(persistedReducer, applyMiddleware(...middlewares));
let persistor = persistStore(store);

sagaMiddleware.run(sagas);

export {
    store,
    persistor
}