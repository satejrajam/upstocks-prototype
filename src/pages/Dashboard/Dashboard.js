import React from "react";
import StockChart from "../../components/StockChart/StockChart";
import { useSelector } from "react-redux";
import { getHistoricalData } from "../../selectors/historical.selector";

const Dashboard = (props) => {
  const { stocks } = useSelector((state) => {
    return {
      stocks: getHistoricalData(state),
    };
  });

  return (
    <div id="dashboard">
      <StockChart type={"svg"} data={stocks} />
    </div>
  );
};

export default Dashboard;
