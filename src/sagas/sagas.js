
import { all } from 'redux-saga/effects';
import historicalSaga from './historical.saga';

function* sagas(){
    yield all([
        historicalSaga()
    ])
}

export default sagas;