import { takeEvery, call, put } from "redux-saga/effects";
import {
  GET_HISTORICAL_DATA,
  getHistoricalActionFail,
  getHistoricalActionSuccess,
} from "../actions/historical.actions";
import { fetchHistoricalDataApi } from "../api/api";

function* fetchHistoricalData(action) {
  try {
    const response = yield call(fetchHistoricalDataApi, action.payload);
    yield put(getHistoricalActionSuccess(response));
  } catch (e) {
    yield put(getHistoricalActionFail([]));
  }
}

function* historicalSaga() {
  yield takeEvery(GET_HISTORICAL_DATA, fetchHistoricalData);
}

export default historicalSaga;
