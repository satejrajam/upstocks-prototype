export const getHistoricalData = (state) => {
  return state?.historicalData?.stocks || [];
};
