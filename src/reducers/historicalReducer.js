import {
  GET_HISTORICAL_DATA_SUCCESS,
  GET_HISTORICAL_DATA_FAIL,
} from "../actions/historical.actions";

const initialState = {
  stocks: [],
  fetchedStocks: false,
};

const historicalReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_HISTORICAL_DATA_SUCCESS:
      return { ...state, stocks: [...action.payload], fetchedStocks: true };
    case GET_HISTORICAL_DATA_FAIL:
      return { ...state, stocks: [], fetchedStocks: false };
    default:
      return state;
  }
};

export default historicalReducer;
