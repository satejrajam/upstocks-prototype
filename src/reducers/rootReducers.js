import { combineReducers } from 'redux';
import appStateReducer from './appStateReducer';
import historicalReducer from './historicalReducer';

export default combineReducers({
    appState: appStateReducer,
    historicalData: historicalReducer
})