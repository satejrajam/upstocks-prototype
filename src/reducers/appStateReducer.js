const initialState = {
    loading: false,
}

const appStateReducer = (state = initialState, action) => {
    switch(action.type){
        default:
            return state
    }
}

export default appStateReducer;

