import axios from "axios";
import { timeParse } from "d3-time-format";

let d3 = require("d3-dsv");

const API = axios.create({
  baseURL: "http://kaboom.rksv.net/api",
});

const fetchHistoricalDataApi = (interval = 1) => {
  return new Promise((res, rej) => {
    API.get(`/historical?interval=${interval}`)
      .then((data) => {
        const historicalArr = [];
        data.data.map((item) => {
          d3.csvParseRows(item, function (d, i) {
            historicalArr.push({
              timestamp: new Date(d[0] * 1000),
              open: Number(d[1]),
              high: Number(d[2]),
              low: Number(d[3]),
              close: Number(d[4]),
              volume: Number(d[5]),
            });
          });
        });

        res(historicalArr);
      })
      .catch((e) => {
        rej(e);
      });
  });
};

export { fetchHistoricalDataApi };
